<?php

namespace App\Http\Livewire;

use Livewire\Component;

class LivewireDatatables extends Component
{
    public function render()
    {
        return view('livewire.livewire-datatables');
    }
}
