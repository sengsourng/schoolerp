<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class AccountDashboardComponent extends Component
{
    public function render()
    {
        return view('livewire.account.account-dashboard-component');
    }
}
