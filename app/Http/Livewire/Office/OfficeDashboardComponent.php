<?php

namespace App\Http\Livewire\Office;

use Livewire\Component;

class OfficeDashboardComponent extends Component
{
    public function render()
    {
        return view('livewire.office.office-dashboard-component');
    }
}
