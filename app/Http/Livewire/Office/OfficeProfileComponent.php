<?php

namespace App\Http\Livewire\Office;

use Livewire\Component;

class OfficeProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.office.office-profile-component');
    }
}
