<?php

namespace App\Http\Livewire\Manager;

use Livewire\Component;

class ManagerProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.manager.manager-profile-component');
    }
}
