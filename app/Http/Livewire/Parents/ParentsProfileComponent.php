<?php

namespace App\Http\Livewire\Parents;

use Livewire\Component;

class ParentsProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.parents.parents-profile-component');
    }
}
