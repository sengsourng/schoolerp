<?php

namespace App\Http\Livewire\Parents;

use Livewire\Component;

class ParentsDashboardComponent extends Component
{
    public function render()
    {
        return view('livewire.parents.parents-dashboard-component');
    }
}
