<?php

namespace App\Http\Livewire\Teacher;

use Livewire\Component;

class TeacherProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.teacher.teacher-profile-component');
    }
}
