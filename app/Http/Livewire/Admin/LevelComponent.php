<?php

namespace App\Http\Livewire\Admin;


use App\Models\Level;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class LevelComponent extends Component
{
    use WithFileUploads;
    use WithPagination;
    // protected $paginationTheme = 'bootstrap';
    // tailwind
    protected $paginationTheme = 'simple-bootstrap';
    // protected $paginationTheme = 'tailwind';


    public $name;
    public $code;
    public $description;



    public $image;
    public $Level_id;
    public $user_id;
    public $status;
    // public $created_by;

    public $newimage;
    public $isModalOpen = 0;

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetInputFields(){
        $this->name = '';
        $this->code = '';
        $this->description = '';
        $this->image='';
        $this->newimage='';
    }


    public function store()
    {
        // dd("Save");
        $this->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',

            // 'image' => 'required',

        ]);

        if($this->newimage){
            $imageName="level-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
            Storage::path($imageName);
            $filePath = $this->newimage->storeAs('levels', $imageName, 'public');
        }
            $data=([
                'name' => $this->name,
                'code' => $this->code,
                'description' => $this->description,

                'image' => $filePath??null ,
                // 'created_by' => Auth::user()->id,
                'user_id' =>Auth::user()->id,
                'school_id'=>getSchoolID_ByUser(Auth::user()->id)

            ]);

            Level::create($data);

            session()->flash('message', 'Level Created Successfully.');

            $this->resetInputFields();
            $this->emit('userStore'); // Close model to using to jquery
    }

    public function edit($Level_id)
    {
        $this->updateMode = true;
        $Level=Level::where('id',$Level_id)->first();
        $this->Level_id=$Level->id;
        $this->name=$Level->name;
        $this->code=$Level->code;
        $this->description=$Level->description;

        $this->image=$Level->image;
        $this->user_id=$Level->user_id;
        $this->status=$Level->status;
        // $this->created_by=Auth::user()->id;

    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }

    public function setStatus($id,$status)
    {
        $Level = Level::find($id);
        if($status==true){
            $Level->status= false;
        }else{
            $Level->status=true;
        }
        $Level->save();
    }

    public function update()
    {

         $this->validate([
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',

        ]);


        if ($this->Level_id) {
            // dd("Check ". $this->Level_id);
            $Level = Level::find($this->Level_id);
            if($this->newimage){
                if(Storage::disk('public')->exists($Level->image)){
                        Storage::disk('public')->delete($Level->image);
                }

                if($Level->image==null){
                    $imageName="Level-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                    Storage::path($imageName);
                    $filePath = $this->newimage->storeAs('Levels', $imageName, 'public');
                    // $Level->image=$filePath; //"Levels/".$imageName;
                }else{
                    $imageName="Level-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                    Storage::path($imageName);
                    $filePath = $this->newimage->storeAs('Levels', $imageName, 'public');
                    // $Level->image=$filePath; //"profile-photos/".$imageName;
                }

            }else{
                $filePath=$Level->image;
            }


                $Level->name= $this->name;
                $Level->code= $this->code;
                $Level->description= $this->description;

                $Level->image= $filePath??null ;
                $Level->user_id= Auth::user()->id;

                 $Level->save();

            $this->updateMode = false;

            $this->resetInputFields();
            session()->flash('message', 'Users Updated Successfully.');
            $this->emit('userStore'); // Close model to using to jquery


        }
    }


    public function delete($id)
    {
        if($id){
            $Level=Level::where('id',$id)->first();
            Level::where('id',$id)->delete();
            if((Storage::disk('public')->exists($Level->image))){
                Storage::disk('public')->delete($Level->image);
            }
            $this->resetInputFields();
            $this->emit('userStore'); // Close model to using to jquery
        }
    }
    public function formDelete($id)
    {
        if($id){
            $Level=Level::where('id',$id)->first();
            $this->name=$Level->name;
            $this->image=$Level->image;
            $this->Level_id=$Level->id;
        }
    }


    public function render()
    {

        // $levels=Level::paginate(4);
        $levels=Level::orderBy('id','desc')->paginate(4);
        // dd($levels);
        // $levels=Level::orderBy('created_at', 'desc')->get();
        // $levels=Level::latest()->get();
        // dd($levels);

        return view('livewire.admin.level-component',
            ['levels'=>$levels]
        )->layout('layouts.front');
    }
}
