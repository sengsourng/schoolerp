<?php

namespace App\Http\Livewire\Admin;

use App\Models\City;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AdminProfileComponent extends Component
{
    use WithFileUploads;

    public $users;
    public $user;
    public $user_id;
    public $name;
    public $email;
    public $username;
    public $phone;
    public $user_type_id;
    public $profile_photo_path;
    public $newimage ;
    public $updated_at;
    public $created_at;

    public $address;

    public $city_id;

    public function mount($user_id)
    {
        $user=User::findOrFail($user_id);
        $this->user_id=$user_id;
        $this->name=$user->name;
        $this->email=$user->email;
        $this->username=$user->username;
        $this->phone=$user->phone;
        $this->address=$user->address;
        $this->city_id=$user->city_id;
        $this->user_type_id=$user->user_type_id;
        $this->profile_photo_path=$user->profile_photo_path;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'name'  => 'required',
            'email'  => 'required',
            // 'username'  => 'required',

        ]);

        if($this->newimage){
            $this->validateOnly($fields,[
                'newimage' => 'required|mimes:png,jpg,jpeg'
            ]);
        }


    }



    public function updateProfile()
    {

        $this->validate([
            'name'  => 'required',
            'address'  => 'required',
            // 'username'  => 'required|unique',
            // 'image'  => 'required|mimes:png,jpg,jpeg',
        ]);

        if($this->newimage){
            $this->validate([
                'newimage' => 'required|mimes:png,jpg,jpeg'
            ]);
        }


        $user=User::find($this->user_id);
        $user->name=$this->name;
        $user->username=$this->username;
        $user->email=$this->email;
        $user->phone=$this->phone;
        $user->address=$this->address;
        $user->city_id=$this->city_id;

        if($this->newimage){

            if(Storage::disk('public')->exists($user->profile_photo_path)){
                    Storage::disk('public')->delete($user->profile_photo_path);
            }

            if($user->profile_photo_path==null){
                $imageName="user-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                Storage::path($imageName);
                $filePath = $this->newimage->storeAs('profile-photos', $imageName, 'public');
                $user->profile_photo_path=$filePath; //"profile-photos/".$imageName;
            }else{
                $imageName="user-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                Storage::path($imageName);
                $filePath = $this->newimage->storeAs('profile-photos', $imageName, 'public');
                $user->profile_photo_path=$filePath; //"profile-photos/".$imageName;
            }


        }

        $user->save();
        session()->flash('message','user has been updated!');

    }


    public function render()
    {
        $user=User::where('id',Auth::user()->id);
        $cities=City::get();

        return view('livewire.admin.admin-profile-component',
                ['user'=>$user,'cities'=>$cities]
                )->layout('layouts.front');
    }
}
