<?php

namespace App\Http\Livewire\Admin;

use App\Models\City;
use App\Models\User;
use App\Models\School;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class SchoolComponent extends Component
{
    use WithFileUploads;
    use WithPagination;
    // protected $paginationTheme = 'bootstrap';
    // tailwind
    protected $paginationTheme = 'simple-bootstrap';
    // protected $paginationTheme = 'tailwind';

    // public $schools;
    public $name;
    public $short_name;
    public $school_id;
    public $phone;
    public $mobile;
    public $email;
    public $password;
    public $city_id;
    public $address;
    public $director_name;

    public $logo;

    public $user_id;
    public $status;
    public $created_by;

    public $newimage;
    public $isModalOpen = 0;

    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    private function resetInputFields(){
        $this->name = '';
        $this->short_name = '';
        $this->email = '';
        $this->password = '';
        $this->mobile = '';
        $this->phone = '';
        $this->address = '';
        $this->city_id = '';
        $this->director_name = '';
    }


    public function store()
    {
        // dd("Save");
        $this->validate([
            'name' => 'required',
            'short_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'password' => 'required',
            'city_id' => 'required',
            'director_name' => 'required',
            // 'logo' => 'required',

        ]);

        if($this->newimage){
            $imageName="school-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
            Storage::path($imageName);
            $filePath = $this->newimage->storeAs('schools', $imageName, 'public');
        }
            $data=([
                'name' => $this->name,
                'short_name' => $this->director_name,
                'email' => $this->email,
                'mobile' => $this->mobile,
                'phone' => $this->phone,
                'address' => $this->address,
                'password' => $this->password,
                'city_id' => $this->city_id,
                'director_name' => $this->director_name,
                'logo' => $filePath??null ,
                'created_by' => Auth::user()->id,

            ]);

            School::create($data);

            session()->flash('message', 'School Created Successfully.');

            $this->resetInputFields();
            $this->emit('userStore'); // Close model to using to jquery
    }

    public function edit($school_id)
    {
        $this->updateMode = true;
        $school=School::where('id',$school_id)->first();
        $this->school_id=$school->id;
        $this->name=$school->name;
        $this->short_name=$school->short_name;
        $this->email=$school->email;
        $this->phone=$school->phone;
        $this->mobile=$school->mobile;
        $this->address=$school->address;
        $this->city_id=$school->city_id;
        $this->logo=$school->logo;
        $this->user_id=$school->user_id;
        $this->status=$school->status;
        $this->created_by=$school->created_by;
        $this->director_name=$school->director_name;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();

    }

    public function update()
    {

         $this->validate([
            'name' => 'required',
            'short_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'address' => 'required',
            // 'password' => 'required',
            // 'city_id' => 'required',
            'director_name' => 'required',
        ]);


        if ($this->school_id) {
            // dd("Check ". $this->school_id);
            $school = School::find($this->school_id);
            if($this->newimage){

                if(Storage::disk('public')->exists($school->logo)){
                        Storage::disk('public')->delete($school->logo);
                }

                if($school->logo==null){
                    $imageName="school-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                    Storage::path($imageName);
                    $filePath = $this->newimage->storeAs('schools', $imageName, 'public');
                    // $school->logo=$filePath; //"schools/".$imageName;
                }else{
                    $imageName="school-".Carbon::now()->timestamp.'.'. $this->newimage->extension();
                    Storage::path($imageName);
                    $filePath = $this->newimage->storeAs('schools', $imageName, 'public');
                    // $school->logo=$filePath; //"profile-photos/".$imageName;
                }

            }


                $school->name= $this->name;
                $school->short_name= $this->director_name;
                $school->email= $this->email;
                $school->mobile= $this->mobile;
                $school->phone= $this->phone;
                $school->address= $this->address;
                // 'password' => $this->password,
                $school->city_id= $this->city_id;
                $school->director_name= $this->director_name;
                $school->logo= $filePath??null ;
                $school->created_by= Auth::user()->id;

                 $school->save();

            $this->updateMode = false;

            session()->flash('message', 'Users Updated Successfully.');
            $this->resetInputFields();
            $this->emit('userStore'); // Close model to using to jquery


        }
    }


    public function delete($id)
    {
        if($id){
            $school=School::where('id',$id)->first();
            School::where('id',$id)->delete();
            if((Storage::disk('public')->exists($school->logo))){
                Storage::disk('public')->delete($school->logo);
            }
            $this->resetInputFields();
            $this->emit('userStore'); // Close model to using to jquery
        }
    }
    public function formDelete($id)
    {
        if($id){
            $school=School::where('id',$id)->first();
            $this->name=$school->name;
            $this->logo=$school->logo;
            $this->school_id=$school->id;
        }
    }




    public function render()
    {
        // ::paginate(4)
        // $schools=School::orderBy('id','desc')->get()->paginate(10);
        $schools=School::paginate(10);
        $cities=City::get();

        return view('livewire.admin.school-component',
            ['schools'=>$schools,'cities'=>$cities]
            )->layout('layouts.front');
    }
}
