<?php

namespace App\Http\Livewire\Student;

use Livewire\Component;

class StudentProfileComponent extends Component
{
    public function render()
    {
        return view('livewire.student.student-profile-component');
    }
}
