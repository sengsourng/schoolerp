<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;
    protected $table="schools";

    protected $fillable=([
        'name','short_name','mobile','phone','address','city_id','director_name','logo',
        'user_id','status','created_by','email'
    ]);

    public function users()
    {
       return $this->hasMany(User::class);
    }
    public  function checkUser($user_id=null)
    {
        if($user_id !=null){
            $user=User::where('id','=',$user_id)->first();
            return $user->name;
        }else{
            return "No User";
        }

        // dd($user->name);
        // return "Hello";
    }
}
