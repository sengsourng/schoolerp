<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->unsignedBigInteger('batch_id')->references('id')->on('batches')->onDelete('cascade');
            $table->unsignedBigInteger('academic_year_id')->references('id')->on('academic_years')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->string('name')->nullable();

            $table->integer('divide_point')->default(6);

            $table->string('room_no')->nullable();
            $table->string('rank')->nullable();
            $table->string('capacity')->nullable();
            $table->bigInteger('class_teacher_id')->nullable();
            $table->date('state_date')->nullable();
            $table->date('end_date')->nullable();
            $table->tinyInteger('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sections');
    }
}
