<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->unsignedBigInteger('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->unsignedBigInteger('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->unsignedBigInteger('academic_year_id')->references('id')->on('academic_years')->onDelete('cascade');
            $table->unsignedBigInteger('batch_id')->references('id')->on('batches')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();

            $table->unsignedBigInteger('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
            $table->string('teacher_name')->default('No Teacher');
            $table->tinyInteger('status')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
