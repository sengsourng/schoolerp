<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $city_name=$this->faker->unique()->words($nb=4,$asText=true);
        $slug=Str::slug($city_name,"-");
        $imageName='city_'. $this->faker->unique()->numberBetween(1,24). '.jpg';
        return [
             'name'=> $city_name,
             'slug' => $slug,
             'note' => $this->faker->text(20),
             'image'  => $imageName,
             'thumbnail' => $imageName,
        ];
    }
}
