<?php

namespace Database\Seeders;

use App\Models\UserType;
use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::create([
            'name'          =>  "រដ្ឋបាលគ្រប់គ្រង",
            'slug'   =>   "admin",
            'note'   =>   "Admin",
            'image'   =>   "admin.png",
            'status'   =>   true,

        ]);
        UserType::create([
            'name'          =>  "អ្នកគ្រប់គ្រង",
            'slug'   =>   "manager",
            'note'   =>   "Manager",
            'image'   =>   "manager.png",
            'status'   =>   true,

        ]);
        UserType::create([
            'name'          =>  "គ្រូបច្ចេកទេស",
            'slug'   =>   "teacher",
            'note'   =>   "Teacher",
            'image'   =>   "teacher.png",
            'status'   =>   true,
        ]);
        UserType::create([
            'name'          =>  "សិស្ស និស្សិត",
            'slug'   =>   "student",
            'note'   =>   "Student",
            'image'   =>   "student.png",
            'status'   =>   true,
        ]);
    }
}
