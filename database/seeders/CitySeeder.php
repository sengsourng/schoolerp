<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'name'          =>  "ភ្នំពេញ",
            'slug'   =>   "phnom-penh",
            'note'   =>   "Phnom Penh",
            'image'   =>   "phnom-penh.jpg",
            'thumbnail'   =>   "phnom-penh.jpg",
        ]);

        City::create([
            'name'          =>  "សៀមរាប",
            'slug'   =>   "siem-reap",
            'note'   =>   "Siem Reap",
            'image'   =>   "siemreap.jpg",
            'thumbnail'   =>   "siemreap.jpg",
        ]);

        City::create([
            'name'          =>  "បាត់ដំបង",
            'slug'   =>   "battambang",
            'note'   =>   "Battambang",
            'image'   =>   "battambang.jpg",
            'thumbnail'   =>   "battambang.jpg",
        ]);

    }
}
