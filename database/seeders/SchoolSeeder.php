<?php

namespace Database\Seeders;

use App\Models\School;
use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
        	'name' => 'វិទ្យាស្ថានពហុបច្ចេកទេសភូមិភាគតេជោសែនសៀមរាប',
        	'short_name' => 'RPITSSR',
            'phone' => '099887766',
            'mobile' => '098887766',
            'email' => 'info@rpitssr.edu.kh',
        	'city_id' => 1,
            'address' => 'បន្ទាយចាស់ ស្លរក្រាម ក្រុងសៀមរាប សៀមរាប',
            'director_name' => 'លោកស្រី ផង់ ពុទ្ធី',
            'user_id' => 1,
            'created_by' => 1,
            'logo' => 'rpitssr_logo.png',
        ]);
    }
}
