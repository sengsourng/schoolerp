<?php

namespace Database\Seeders;

use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $admin= User::create([
            'name'          =>  "Admin User",
            'email'   =>   "admin@gmail.com",
            'username'   =>   "admin",
            'password'   =>   Hash::make("12345678"),
            'phone'  => "099771244",
            'city_id'  => 1,
            'user_type_id'  => 1,
        ]);

        Team::create([
            'user_id'          =>  $admin->id,
            'name'   =>   "Team 's " . $admin->name,
            'personal_team'   =>  true,
        ]);


        $manager=User::create([
            'name'          =>  "Manager User",
            'email'   =>   "manager@gmail.com",
            'username'   =>   "manager",
            'password'   =>   Hash::make("12345678"),
            'phone'  => "099771245",
            'city_id'  => 3,
            'user_type_id'  =>2,

        ]);

        Team::create([
            'user_id'          =>  $manager->id,
            'name'   =>   "Team 's " . $manager->name,
            'personal_team'   =>  true,
        ]);


        $teacher=User::create([
            'name'          =>  "Teacher User",
            'email'   =>   "teacher@gmail.com",
            'username'   =>   "teacher",
            'password'   =>   Hash::make("12345678"),
            'phone'  => "099771246",
            'city_id'  => 2,
            'user_type_id'  => 3,
        ]);
        Team::create([
            'user_id'          =>  $teacher->id,
            'name'   =>   "Team 's " . $teacher->name,
            'personal_team'   =>  true,
        ]);



        $student=User::create([
            'name'          =>  "Student User",
            'email'   =>   "student@gmail.com",
            'username'   =>   "student",
            'password'   =>   Hash::make("12345678"),
            'phone'  => "099771247",
            'city_id'  => 2,
            'user_type_id'  => 4,
        ]);

        Team::create([
            'user_id'          =>  $student->id,
            'name'   =>   "Team 's " . $student->name,
            'personal_team'   =>  true,
        ]);

    }
}
