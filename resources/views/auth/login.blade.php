{{-- <x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout> --}}


<x-base-layout>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="p-4 rounded">
                        <div class="text-center">
                            <h3 class="">ចូលប្រើ​ប្រព័ន្ធ</h3>
                            <p>អ្នកមិនទាន់មាន​ គណនីនៅឡើយ ? <a href="{{route('register')}}">ចុះឈ្មោះទីនេះ</a>
                            </p>
                        </div>
                        <div class="form-body">
                            <form class="row g-3" method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="col-12">
                                    <label for="inputEmailAddress" class="form-label">អ៊ីម៉ែល</label>
                                    <input type="email" class="form-control" id="inputEmailAddress" placeholder="Email Address" name="email" :value="old('email')" required autofocus>
                                </div>
                                <div class="col-12">
                                    <label for="inputChoosePassword" class="form-label">ពាក្យសំង៉ាត់</label>
                                    <div class="input-group" id="show_hide_password">

                                        <input type="password" class="form-control border-end-0" id="inputChoosePassword" value="12345678" placeholder="Enter Password" name="password" required autocomplete="current-password">
                                        <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                        <label class="form-check-label" for="flexSwitchCheckChecked">ចងចាំខ្ញុំ</label>
                                    </div>
                                </div>
                                <div class="col-md-6 text-end">	<a href="authentication-forgot-password.html">ភ្លេចពាក្យ​សំង៉ាត់ ?</a>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>ចូល​ប្រើ</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="login-separater text-center mb-4"> <span>ឬ ចូលប្រើ​ដោយ ប្រើ បណ្តាញសង្គម</span>
                            <hr/>
                        </div>

                        <div class="d-grid">
                            <a class="btn my-4 shadow-sm btn-white" href="javascript:;">
                                <span class="d-flex justify-content-center align-items-center">
                                    <img class="me-2" src="assets/images/icons/search.svg" width="16" alt="Image Description">
                                    <span>Sign in with Google</span>
                                </span>
                            </a> <a href="javascript:;" class="btn btn-facebook">
                                <i class="bx bxl-facebook"></i>Sign in with Facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 d-none d-lg-block">
            <img style="width: 100%;" src="{{asset('assets/login_logo.gif')}}" alt="">
        </div>
    </div>


    @push('scripts')
    <!--Password show & hide js -->
    <script>
        $(document).ready(function () {
            $("#show_hide_password a").on('click', function (event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("bx-hide");
                    $('#show_hide_password i').removeClass("bx-show");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("bx-hide");
                    $('#show_hide_password i').addClass("bx-show");
                }
            });
        });
    </script>

    @endpush
</x-base-layout>
