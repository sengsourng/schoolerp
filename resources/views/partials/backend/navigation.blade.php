<div class="nav-container">
    <div class="mobile-topbar-header">
        <div>
            <img src="{{asset('assets/logo.png')}}" style="height: 48px;" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">វិទ្យាស្ថាន</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-first-page'></i>
        </div>
    </div>
    <nav class="topbar-nav">
        <ul class="metismenu" id="menu">
            <li class="dropdown">
                <a href="#" class="dropbtn">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">អំពីវិទ្យាស្ថាន</div>
                </a>
                <ul class="dropdown-content">
                    <li>
                        <a href="#"><i class="bx bx-right-arrow-alt"></i>Backgroun</a>
                    </li>
                    <li>
                        <a href="#"><i class="bx bx-right-arrow-alt"></i>Question & Answer</a>
                    </li>
                    <li>
                        <a href="#"><i class="bx bx-right-arrow-alt"></i>Privacy Policy</a>
                    </li>
                    <li>
                        <a href="#"><i class="bx bx-right-arrow-alt"></i>Vission, Mission</a>
                    </li>
                    <li>
                        <a href="#"><i class="bx bx-right-arrow-alt"></i>Strategic Gold</a>
                    </li>

                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" class="dropbtn">
                    <div class="parent-icon"><i class="bx bx-category"></i>
                    </div>
                    <div class="menu-title">កម្មវិធីសិក្សា</div>
                </a>
                <ul class="dropdown-content">
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Facalty Accounting</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Facalty ICT</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Facalty Electronic</a>
                    </li>

                </ul>
            </li>
            <li class="dropdown">
                <a class="dropbtn" href="javascript:;">
                    <div class="parent-icon"><i class="bx bx-line-chart"></i>
                    </div>
                    <div class="menu-title">ការចុះឈ្មោះ</div>
                </a>
                <ul class="dropdown-content">
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Adminission Infomation</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Enrollment</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Scholarship</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropbtn" href="javascript:;">
                    <div class="parent-icon"><i class='bx bx-bookmark-heart'></i>
                    </div>
                    <div class="menu-title">ការស្រាវជ្រាវ</div>
                </a>
                <ul class="dropdown-content">
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Publication</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Research</a>
                    </li>
                    <li> <a href="#"><i class="bx bx-right-arrow-alt"></i>Consultancy</a>
                    </li>

                </ul>
            </li>
            <li class="dropdown">
                <a  href="javascript:;"  class="dropbtn">
                    <div class="parent-icon"><i class="bx bx-group"></i>
                    </div>
                    <div class="menu-title">គ្រប់គ្រងសិស្ស</div>
                </a>
                <ul class="dropdown-content">
                    <li> <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>រកការងារធ្វើ</a>
                    </li>
                    <li> <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ទំនាក់ទំនងក្រុមហ៊ុន</a>
                    </li>
                    <li> <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ផ្តល់កន្លែងស្នាក់នៅ</a>
                    </li>
                    <li> <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ចុះកម្មសិក្សា</a>
                    </li>

                </ul>
            </li>

            <li class="dropdown">
                <a  href="{{route('admin.seetings')}}" class="dropbtn">
                    <div class="parent-icon"><i class="fa fa-cogs" aria-hidden="true"></i>
                    </div>
                    <div class="menu-title">ការកំណត់</div>
                </a>
                <ul class="dropdown-content">
                    <li>
                        <a href="#" target="_blank"><i class="fa fa-sliders" aria-hidden="true"></i> ស្លាយមុខ</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="fa fa-user-secret" aria-hidden="true"></i>សិទ្ធិ​អ្នកប្រើប្រាស់</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ឆ្នាំសិក្សា</a>
                    </li>

                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ជំនាន់</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ឆមាស</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ជំនាញ</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>ប្រភេទជំនាញ</a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><i class="bx bx-right-arrow-alt"></i>មុខវិជ្ជា</a>
                    </li>

                    <li>
                        <a href="{{route('admin.levels')}}"><i class="bx bx-right-arrow-alt"></i>កំរិតសិក្សា</a>
                    </li>
                    <li>
                        <a href="{{route('admin.schools')}}"><i class="fa fa-building"></i>គ្រប់គ្រងគ្រឹះស្ថាន</a>
                    </li>

                </ul>
            </li>




        </ul>
    </nav>
</div>
