<header>
    <div class="topbar d-flex align-items-center">
        <nav class="navbar navbar-expand">
            <div class="topbar-logo-header">
                <div class="">
                    {{-- <img src="{{asset('assets/logo.png')}}" class="logo-icon" alt="logo icon"> --}}
                    <a href="{{route('home')}}"><img src="{{asset('assets/banner_logo.png')}}" style="height: 100%;" alt="logo icon"></a>
                </div>
                {{-- <div class="">
                    <h4 class="logo-text">RPITSSR</h4>
                </div> --}}
            </div>
            <div class="mobile-toggle-menu"><i class='bx bx-menu'></i></div>
            <div class="top-menu-left d-none d-lg-block ps-3">
                <ul class="nav">

                  <li class="text-white my-1" style="font-size: 18px;"><i class="fa fa-calendar"></i> {{getCurrentDate(now())}}</li>

              </ul>
             </div>
            <div class="search-bar flex-grow-1">
                <div class="position-relative search-bar-box">
                    <input type="text" class="form-control search-control" placeholder="Type to search..."> <span class="position-absolute top-50 search-show translate-middle-y"><i class='bx bx-search'></i></span>
                    <span class="position-absolute top-50 search-close translate-middle-y"><i class='bx bx-x'></i></span>
                </div>
            </div>
            <div class="top-menu ms-auto">
                <ul class="navbar-nav align-items-center">
                    <ul class="navbar-nav align-items-center">
                        <li class="nav-item mobile-search-icon">
                            <a class="nav-link" href="#">	<i class='bx bx-search'></i>
                            </a>
                        </li>
                        <li class="nav-item dropdown dropdown-large">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">	<i class='bx bx-category'></i>
                                <span>ផ្សេងៗ</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <div class="row row-cols-3 g-3 p-3">
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-cosmic text-white"><i class='bx bx-group'></i>
                                        </div>
                                        <div class="app-title">សាស្រ្តាចារ្យ</div>
                                    </div>
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-burning text-white"><i class='fa fa-user-md'></i>
                                        </div>
                                        <div class="app-title">សុខភាព</div>
                                    </div>
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-lush text-white"><i class='bx bx-home'></i>
                                        </div>
                                        <div class="app-title">ការស្នាក់នៅ</div>
                                    </div>
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-kyoto text-dark"><i class='bx bx-notification'></i>
                                        </div>
                                        <div class="app-title">ពត៌មាន</div>
                                    </div>
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-blues text-dark"><i class='bx bx-file'></i>
                                        </div>
                                        <div class="app-title">ទាញយកមេរៀន</div>
                                    </div>
                                    <div class="col text-center">
                                        <div class="app-box mx-auto bg-gradient-moonlit text-white"><i class='fa fa-industry'></i>
                                        </div>
                                        <div class="app-title">សិស្សមានការងារ</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown dropdown-large">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="alert-count">4</span>
                                <i class='bx bx-bell'></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="javascript:;">
                                    <div class="msg-header">
                                        <p class="msg-header-title">ទំនាក់ទំនង</p>
                                        <p class="msg-header-clear ms-auto">ទាក់ទងពត៌មានសាលា</p>
                                    </div>
                                </a>
                                <div class="header-notifications-list">
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="notify bg-light-primary text-primary"><i class="bx bx-group"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h4 class="msg-name">ពត៌មានពីការចូលរៀន</h4>
                                                <p class="msg-info">092771244</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="notify bg-light-danger text-danger"><i class="bx bx-cart-alt"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">រៀនជំនាញ វិទ្យាសាស្រ្តកុំព្យទ័រ</h6>
                                                <p class="msg-info">093771244</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="notify bg-light-success text-success"><i class="bx bx-file"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">រៀនសំណង់</h6>
                                                <p class="msg-info">092771244</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="notify bg-light-warning text-warning"><i class="bx bx-send"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">បណ្តឹងតវា ផ្សេងៗ<span class="msg-time float-end">ប្តឹងគ្រូ និង​សាលា</span></h6>
                                                <p class="msg-info">092889977</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown dropdown-large  d-none d-lg ps-3">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <span class="alert-count">8</span>
                                <i class='bx bx-comment'></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a href="javascript:;">
                                    <div class="msg-header">
                                        <p class="msg-header-title">Messages</p>
                                        <p class="msg-header-clear ms-auto">Marks all as read</p>
                                    </div>
                                </a>
                                <div class="header-message-list">
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="user-online">
                                                <img src="{{asset('assets/images/avatars/avatar-1.png')}}" class="msg-avatar" alt="user avatar">
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">Daisy Anderson <span class="msg-time float-end">5 sec
                                            ago</span></h6>
                                                <p class="msg-info">The standard chunk of lorem</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="user-online">
                                                <img src="{{asset('assets/images/avatars/avatar-2.png')}}" class="msg-avatar" alt="user avatar">
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">Althea Cabardo <span class="msg-time float-end">14
                                            sec ago</span></h6>
                                                <p class="msg-info">Many desktop publishing packages</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="user-online">
                                                <img src="{{asset('assets/images/avatars/avatar-3.png')}}" class="msg-avatar" alt="user avatar">
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">Oscar Garner <span class="msg-time float-end">8 min
                                            ago</span></h6>
                                                <p class="msg-info">Various versions have evolved over</p>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item" href="javascript:;">
                                        <div class="d-flex align-items-center">
                                            <div class="user-online">
                                                <img src="{{asset('assets/images/avatars/avatar-3.png')}}" class="msg-avatar" alt="user avatar">
                                            </div>
                                            <div class="flex-grow-1">
                                                <h6 class="msg-name">Katherine Pechon <span class="msg-time float-end">15
                                            min ago</span></h6>
                                                <p class="msg-info">Making this the first true generator</p>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </li>
                    </ul>
                </ul>
            </div>

            @if (Route::has('login'))
                @auth
                    <div class="user-box dropdown">
                        <a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{ Auth::user()->profile_photo_url }}" class="user-img" alt="user avatar">
                            <div class="user-info ps-3">
                                <p class="user-name mb-0">{{ Auth::user()->name }}</p>
                                <p class="designattion mb-0">{{ Auth::user()->user_type->name }}</p>
                            </div>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            {{-- Admin --}}
                            @if (Auth::user()->user_type_id===1)
                                <li>
                                    <a class="dropdown-item" href="{{route('admin.profile',Auth::user()->id)}}"><i class="bx bx-user"></i><span>Profile</span></a>
                                </li>
                            @endif
                            {{-- Teacher --}}
                            @if (Auth::user()->user_type_id===3)
                                <li>
                                    <a class="dropdown-item" href="{{route('teacher.profile',Auth::user()->id)}}"><i class="bx bx-user"></i><span>Profile</span></a>
                                </li>
                            @endif
                            {{-- Student --}}
                            @if (Auth::user()->user_type_id===4)
                                <li>
                                    <a class="dropdown-item" href="{{route('student.profile',Auth::user()->id)}}"><i class="bx bx-user"></i><span>Profile</span></a>
                                </li>
                            @endif




                            <li>
                                <a class="dropdown-item" href="{{route('admin.dashboard')}}"><i class='bx bx-home-circle'></i><span>Dashboard</span></a>
                            </li>


                            <li>
                                <div class="dropdown-divider mb-0"></div>
                            </li>

                            <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class='bx bx-log-out-circle' ></i><span>Logout</span></a></li>


                            <form action="{{route('logout')}}" method="POST" id="logout-form" style="display: none;">
                                @csrf
                            </form>

                        </ul>
                    </div>
                @else

                    <div class="user-box dropdown">
                        <a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{asset('assets/logo.png')}}" class="user-img" alt="user avatar">
                            <div class="user-info ps-3">
                                <p class="user-name mb-0">ចូល​ប្រើ​ និង​ចុះឈ្មោះ</p>
                                <p class="designattion mb-0">Loging & Register</p>
                            </div>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li>
                                <a class="dropdown-item" href="{{ route('login') }}"><i class="bx bx-user"></i><span>ចូលប្រើ</span></a>
                            </li>

                            @if (Route::has('register'))
                            <li><a class="dropdown-item" href="{{ route('register') }}"><i class="bx bx-cog"></i><span>ចុះឈ្មោះ</span></a>
                            </li>
                                {{-- <a href="{{ route('register') }}" class="btn btn-light radius-10 px-2"><i class="bx bx-user"></i> Register</a> --}}
                            @endif



                        </ul>
                    </div>

                @endauth

            @endif

        </nav>
    </div>
</header>
