<footer id="footer">

    <div class="footer-top">
    <div class="container">
        <div class="row">

        <div class="col-lg-3 col-md-6 footer-contact">
            {{-- <h3>វិទ្យាស្ថានពហុបច្ចេកទេសភូមិភាគតេជោសែនសៀមរាប</h3> --}}
            <img src="{{asset('assets/banner_logo.png')}}" alt="">
            <p>
            ផ្លូវជាតិលេខ៦,
            បន្ទាយចាស់ ស្លក្រាម<br>
            ក្រុងសៀមរាប កម្ពុជា <br><br>
            <strong>Phone:</strong> +855 92771244<br>
            <strong>Email:</strong> info@rpitssr.edu.kh<br>
            </p>
        </div>

        <div class="col-lg-2 col-md-6 footer-links">
            <h4>ទំព័រសំខាន់ៗ</h4>
            <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="#">ទំព័រដើម</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">អំពីវិទ្យាស្ថាន</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">សេវាកម្ម</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links">
            <h4>ជំនាញសំខាន់ៗ</h4>
            <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
        </div>

        <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>ចូលរួមទីនេះដើម្បីតាមដាន</h4>
            <p>បំពេញពត៌មាន Email របស់អ្នក​ដើម្បី​តាមដាន​ ពត៌មានថ្មីៗពីសាលា</p>
            <form action="" method="post">
            <input type="email" name="email" class="form-control"><input type="submit" value="Subscribe" class="btn btn-success">
            </form>
        </div>

        </div>
    </div>
    </div>

    <div class="container d-md-flex py-4">

    <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
        &copy; Copyright <strong><span>RPITSSR</span></strong>. All Rights Reserved
        </div>

    </div>
    <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
    </div>
    </div>
</footer>
