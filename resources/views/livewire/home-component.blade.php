<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">
            <div class="card-body">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="col-md-4 carousel-item active carousel-item-start">
                            <img src="{{asset('slide/slide-1.jpg')}}" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item carousel-item-next carousel-item-start">
                            <img src="{{asset('slide/slide-2.jpg')}}" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('slide/slide-3.jpg')}}" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-bs-slide="prev">	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-bs-slide="next">	<span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="col d-flex">
            <div class="card radius-10 w-100">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h3 class="font-weight-bold mb-0">ការិយាល័យ និង​ដេប៉ាតឺម៉ង់</h3>
                        </div>

                    </div>
                </div>
                    <div class="best-selling-products p-3 mb-3">
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-1.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ការិយាល័យរដ្ឋបាល</h6>
                                <p class="mb-0 text-secondary">លោកស្រី ឃ្លាំង ចិន្តា</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៥ នាក់</p>
                        </div>
                        <hr/>
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-2.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ការិយាល័យអប់រំ</h6>
                                <p class="mb-0 text-secondary">លោក តូ សុផាន់ណារ៉ា</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៨ នាក់</p>
                        </div>
                        <hr/>
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-3.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ការិយាល័យទីផ្សារ</h6>
                                <p class="mb-0 text-secondary">លោក ប៊ី សារ័ត្ន</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៥ នាក់</p>
                        </div>
                        <hr/>
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-4.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ការិយាល័យហិរញ្ញវត្ថុ</h6>
                                <p class="mb-0 text-secondary">លោក សោ សុបុណ្យ</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៦ នាក់</p>
                        </div>
                        <hr/>
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-5.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ដេប៉ាតឺម៉ង់ អគ្គិសនី</h6>
                                <p class="mb-0 text-secondary">លោក ឆែម រ័ត្ន</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៨នាក់</p>
                        </div>
                        <hr/>
                        <div class="d-flex align-items-center">
                            <div class="product-img">
                                <img src="{{asset('assets/images/avatars/avatar-6.png')}}" class="rounded user-img" alt="" />
                            </div>
                            <div class="ps-3">
                                <h6 class="mb-0 font-weight-bold">ដេប់ាតឺម៉ង់ វិទ្យាសាស្រ្តកុំព្យូទ័រ</h6>
                                <p class="mb-0 text-secondary">លោក កុល សេរីរឹទ្ធ</p>
                            </div>
                            <p class="ms-auto mb-0 text-purple">៤ នាក់</p>
                        </div>

                    </div>
            </div>
       </div>
    </div>
</div>

<div class="row row-cols-1 row-cols-md-3 row-cols-xl-5">
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto bg-light-primary text-primary mb-3">
                        {{-- <i class="bx bxl-facebook-square"></i> --}}
                        <i class="fa fa-users" aria-hidden="true"></i>
                        {{-- <i class="bx bxl-user-square"></i> --}}
                    </div>
                    <h4 class="my-1">103</h4>
                    <p class="mb-0 text-secondary">សិស្សបរិញ្ញាបត្រ</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto bg-light-danger text-danger mb-3">
                        {{-- <i class="bx bxl-twitter"></i> --}}
                        <i class="fa fa-users" aria-hidden="true"></i>

                    </div>
                    <h4 class="my-1">221</h4>
                    <p class="mb-0 text-secondary">សិស្ស​បរិញ្ញាបត្រ​រង</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto bg-light-info text-info mb-3">
                        {{-- <i class="bx bxl-linkedin-square"></i> --}}
                        <i class="fa fa-users" aria-hidden="true"></i>

                    </div>
                    <h4 class="my-1">16</h4>
                    <p class="mb-0 text-secondary">សញ្ញាបត្របច្ចេកទេស C3</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto bg-light-success text-success mb-3">
                        {{-- <i class="bx bxl-youtube"></i> --}}
                        <i class="fa fa-users" aria-hidden="true"></i>

                    </div>
                    <h4 class="my-1">24</h4>
                    <p class="mb-0 text-secondary">សញ្ញាបត្របច្ចេកទេស C2</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto bg-light-warning text-warning mb-3">
                        {{-- <i class="bx bxl-dropbox"></i> --}}
                        <i class="fa fa-users" aria-hidden="true"></i>

                    </div>
                    <h4 class="my-1">50</h4>
                    <p class="mb-0 text-secondary">សញ្ញាបត្របច្ចេកទេស C1</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card-body">
    <div class="d-flex align-items-center">
        <div>
            <h3 class="font-weight-bold mb-0">លក្ខខណ្ឌចុះឈ្មោះចូលរៀន</h3>
        </div>
    </div>
</div>

<div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4">
    <div class="col">
        <div class="card">
            <img class="zoom" src="assets/images/gallery/01.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">ថ្នាក់បរិញ្ញាបត្រ</h5>
                <p class="card-text">បេក្ខជនត្រូវមានសញ្ញាបត្រមធ្យមសិក្សាទុតិយភូមិ ឬសញ្ញាបត្របច្ចេកទេស និងវិជ្ជាជីវៈ៣ ឬសញ្ញាបត្រដែលមានតម្លៃស្មើ</p>
                {{-- <a href="javascript:;" class="btn btn-primary btn-block">Detail</a> --}}
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <img class="zoom" src="assets/images/gallery/02.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">ថ្នាក់បរិញ្ញាបត្ររង</h5>
                <p class="card-text">បេក្ខជនត្រូវមានសញ្ញាបត្រមធ្យមសិក្សាទុតិយភូមិ (ធ្លាក់បាក់ឌុប) ឬសញ្ញាបត្របច្ចេកទេស និងវិជ្ជាជីវៈ៣ ឬសញ្ញាបត្រដែលមានតម្លៃស្មើ</p>
                {{-- <a href="javascript:;" class="btn btn-danger">Detail</a> --}}
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <img class="zoom" src="assets/images/gallery/03.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">វិញ្ញាបនបត្រវិជ្ជាជីវៈ</h5>
                <p class="card-text">បេក្ខជនដែលបានរៀនចប់មធ្យមសិក្សាបឋមភូមិ(ថ្នាក់ទី៩) ឬ បានបញ្ចប់ថ្នាក់ស្ពានចម្លងជំនាញថ្នាក់ទី៨ និងទី៩ ឬកម្រិតសមមូល</p>
                {{-- <a href="javascript:;" class="btn btn-success">Go Detail</a> --}}
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <img class="zoom" src="assets/images/gallery/04.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">វគ្គខ្លីបច្ចេកទេសផ្សេងៗ</h5>
                <p class="card-text">បេក្ខជនអាចចុះឈ្មោះចូលរៀន​ជំនាញផ្សេងៗ​ដែល​ជា​វគ្គខ្លី​ក៣ខែចុះក្រោម​។</p>
                {{-- <a href="javascript:;" class="btn btn-warning">Go Detail</a> --}}
            </div>
        </div>
    </div>


</div>




<section class="container pb-4 pt-2 pt-sm-0 pt-md-2 pb-sm-5">
    <h2 class="text-center">ក្រុមហ៊ុន និង​អង្គារដៃគូ</h2>
    <p class="text-muted pb-4 text-center">ខាងក្រោម​ជាដៃគូសំខានៗរបស់វិទ្យាស្ថាន</p>
    <div class="row pb-2 pb-sm-0 pb-md-3">
        @for ($i = 1; $i <=12; $i++)
            @if ($i<10)
                <div class="col-md-3 col-sm-4 col-6 mt-2"><a class="d-block bg-white shadow-sm rounded-3 py-3 py-sm-4 mb-grid-gutter" href="food-delivery-single.html"><img class="d-block mx-auto" src="{{asset('sponsers/0'. $i .".png")}}" style="width: 150px;" alt="Brand"></a></div>
            @else
                <div class="col-md-3 col-sm-4 col-6 mt-2"><a class="d-block bg-white shadow-sm rounded-3 py-3 py-sm-4 mb-grid-gutter" href="food-delivery-single.html"><img class="d-block mx-auto" src="{{asset('sponsers/'. $i .".png")}}" style="width: 150px;" alt="Brand"></a></div>
            @endif

        @endfor


    </div>
  </section>
