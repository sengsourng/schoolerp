<div class="col">
    <!-- Modal -->
    <form>
        <div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">ពត៌មានលុបទិន្ន​ន័យ</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        @if (Storage::disk('public')->exists($this->image))
                                            <img style="width: 100%;"  src="{{asset('storage/'. $this->image)}}" class="p-1" alt="">
                                        @else
                                            <img style="width: 100%;" src="{{asset('assets/images/icons/shoes.png')}}" class="p-1" alt="">
                                        @endif
                                    </div>
                                    <div class="col-md-9">
                                        <h4 class="text-left">តើអ្នក​ពិតជាចង់លុបមែនឬ?</h4>
                                        <h6 class="text-left text-danger">{{$name}}</h6>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" wire:click="closeModalPopover()" class="btn btn-danger" data-bs-dismiss="modal"><span class="fa fa-close"></span> បោះបង់</button>
                                <button type="button" wire:click.prevent="delete({{$Level_id}})" class="btn btn-info text-white close-modal"><span class="fa fa-check"></span> បាទ/ចាស</button>
                            </div>

                </div>
            </div>
        </div>
    </form>
</div>
