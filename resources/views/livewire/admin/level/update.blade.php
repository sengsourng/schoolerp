<div class="col">
    <!-- Modal -->
    <form>
        <div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">បង្កើត សាលា</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                            <div class="col-md-8">
                                    <div class="form-group mb-2">
                                        <label for="code" class="mb-2">កូដកំរិត</label>
                                        <input class="form-control" type="text" wire:model="code"
                                            id="code" placeholder="ដាក់ឈ្មោះពេញនាយក">
                                        @error('code') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="form-group mb-2">
                                        <label for="name" class="mb-2">ឈ្មោះកំរិត</label>
                                        <input class="form-control" type="text" wire:model="name"  id="name" placeholder="ឈ្មោះសាលា..">
                                        @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="form-group mb-2">
                                        <label for="description" class="mb-2">អធិប្បាយពន្យល់</label>
                                        <input class="form-control" type="text" wire:model="description"  id="description" placeholder="អធិប្បាយពន្យល់">
                                        @error('description') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                            </div>
                            {{-- Right --}}
                            <div class="col-md-4">

                                <div class="col-sm-9 text-secondary">
                                    <input type='file' id="newimage" class="form-control-file" style="display:none" name="image" wire:model="newimage">
                                    {{-- <input id="newimage" type="file" class="form-control-file" name="image" wire:model="newimage"> --}}
                                    @error('newimage') <p class="text-danger">{{$message}}</p> @enderror



                                    @if ($newimage)
                                        <img  class="rounded-squre p-1" width="210" src="{{$newimage->temporaryUrl()}}" alt="" style="cursor: pointer;" onclick="document.getElementById('newimage').click()">

                                    @else
                                        @if (Storage::disk('public')->exists($this->image))
                                            <div class="rounded pull-left">
                                                <img  src="{{asset('storage/'. $this->image)}}" alt="{{$name}}"
                                                    style="cursor: pointer;" onclick="document.getElementById('newimage').click()"
                                                    class="rounded-squre p-1" width="210"
                                                    >
                                            </div>
                                            {{-- <img height="64"  src="{{asset('storage/'. $this->image)}}" class="p-1" alt=""> --}}
                                        @else
                                            <div class="rounded pull-left">
                                                <img  src="{{asset('assets/images/icons/school.png')}}" alt="{{$name}}"
                                                    style="cursor: pointer;" onclick="document.getElementById('newimage').click()"
                                                    class="rounded-squre p-1" width="210"
                                                    >
                                            </div>
                                        @endif

                                    @endif
                                </div>



                            </div>
                            </div>

                        </div>
                            <div class="modal-footer">
                                <button type="button" wire:click="closeModalPopover()" class="btn btn-danger" data-bs-dismiss="modal"><span class="fa fa-close"></span> បិត</button>
                                <button type="button" wire:click.prevent="update()" class="btn btn-info text-white close-modal"><span class="fa fa-save"></span> ធ្វើបច្ចុប្បន្នភាព</button>
                            </div>

                </div>
            </div>
        </div>
    </form>
</div>
