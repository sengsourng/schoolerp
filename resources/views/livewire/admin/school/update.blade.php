<div class="col">
    <!-- Modal -->
    <form>
        <div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">បង្កើត សាលា</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                            <div class="modal-body">
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group mb-2">
                                            <label for="director_name" class="mb-2">ឈ្មោះនាយក</label>
                                            <input class="form-control" type="text" wire:model="director_name"
                                                id="director_name" placeholder="ដាក់ឈ្មោះពេញនាយក">
                                            @error('director_name') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>

                                        <div class="form-group mb-2">
                                            <label for="name" class="mb-2">ឈ្មោះពេញ របស់សាលា</label>
                                            <input class="form-control" type="text" wire:model="name"  id="name" placeholder="ឈ្មោះសាលា..">
                                            @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>

                                        <div class="form-group mb-2">
                                            <label for="short_name" class="mb-2">ឈ្មោះខ្លីរបស់សាលា</label>
                                            <input class="form-control" type="text" wire:model="short_name"  id="short_name" placeholder="ឈ្មោះសាលា..">
                                            @error('short_name') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>

                                        <div class="form-group mb-2">
                                            <label for="email" class="mb-2">អ៊ីម៉ែល</label>
                                            <input class="form-control" type="email" wire:model="email"  id="email" placeholder="yourname@gmail.com">
                                            @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="mobile" class="mb-2">ទូរស័ព្ទចល័ត</label>
                                            <input class="form-control" type="text" wire:model="mobile"  id="mobile" placeholder="093889988">
                                            @error('mobile') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>

                                        <div class="form-group mb-2">
                                            <label for="password" class="mb-2">ពាក្យសំងាត់</label>
                                            <input class="form-control" type="password" wire:model="password"  id="password" placeholder="បញ្ចូល​ពាក្យសំង៉ាត់">
                                            @error('password') <span class="text-danger">{{ $message }}</span>@enderror
                                        </div>


                                </div>
                                {{-- Right --}}
                                <div class="col-md-6">
                                    <div class="form-group mb-2">
                                        <label for="phone" class="mb-2">ទូរស័ព្ទសាលា</label>
                                        <input class="form-control" type="text" wire:model="phone"  id="phone" placeholder="098776655">
                                        @error('phone') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="form-group mb-2">
                                        <label for="address" class="mb-2">អាស័យដ្ឋាន</label>
                                        <input class="form-control" type="text" wire:model="address"  id="address" placeholder="ភូមិ ឃុំ ស្រុក ខេត្ត">
                                        @error('address') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>


                                    <div class="form-group mb-2">
                                        <label for="city_id" class="mb-2">ខេត្ត/ក្រុង</label>
                                        <select name="city_id" class="form-control" id="city_id" wire:model="city_id">
                                            @foreach ($cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('city_id') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>



                                    <div class="col-sm-9 text-secondary">
                                        <input type='file' id="newimage" class="form-control-file" style="display:none" name="logo" wire:model="newimage">
                                        {{-- <input id="newimage" type="file" class="form-control-file" name="image" wire:model="newimage"> --}}
                                        @error('newimage') <p class="text-danger">{{$message}}</p> @enderror



                                        @if ($newimage)
                                            <img  class="rounded-squre p-1" width="210" src="{{$newimage->temporaryUrl()}}" alt="" style="cursor: pointer;" onclick="document.getElementById('newimage').click()">

                                        @else
                                            @if (Storage::disk('public')->exists($this->logo))
                                                <div class="rounded pull-left">
                                                    <img  src="{{asset('storage/'. $this->logo)}}" alt="{{$name}}"
                                                        style="cursor: pointer;" onclick="document.getElementById('newimage').click()"
                                                        class="rounded-squre p-1" width="210"
                                                        >
                                                </div>
                                                {{-- <img height="64"  src="{{asset('storage/'. $this->logo)}}" class="p-1" alt=""> --}}
                                            @else
                                                <div class="rounded pull-left">
                                                    <img  src="{{asset('assets/images/icons/school.png')}}" alt="{{$name}}"
                                                        style="cursor: pointer;" onclick="document.getElementById('newimage').click()"
                                                        class="rounded-squre p-1" width="210"
                                                        >
                                                </div>
                                            @endif

                                        @endif
                                    </div>


                                </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" wire:click="closeModalPopover()" class="btn btn-danger" data-bs-dismiss="modal"><span class="fa fa-close"></span> បិត</button>
                                <button type="button" wire:click.prevent="update()" class="btn btn-info text-white close-modal"><span class="fa fa-save"></span> ធ្វើបច្ចុប្បន្នភាព</button>
                            </div>

                </div>
            </div>
        </div>
    </form>
</div>
