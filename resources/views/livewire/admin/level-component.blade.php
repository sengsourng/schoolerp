
<div>
    <div class="card radius-10">
        <div class="card-header border-bottom-0 bg-transparent pt-3">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="font-weight-bold mb-0">បញ្ជីកំរិតសិក្សា</h4>
                </div>
                <div class="ms-auto">
                    <button type="button" class="btn btn-success radius-10" data-bs-toggle="modal" data-bs-target="#CreateModal"><span class="fa fa-plus"></span> បន្ថែមថ្មី</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            @include('livewire.admin.level.create')
            @include('livewire.admin.level.update')
            @include('livewire.admin.level.delete')

            @if (session()->has('message'))
                <div class="alert border-0 border-start border-5 border-success alert-dismissible fade show py-2">
                    <div class="d-flex align-items-center">
                        <div class="font-35 text-success"><i class="bx bxs-check-circle"></i>
                        </div>
                        <div class="ms-3">
                            <h6 class="mb-0 text-success">Success</h6>
                            <div> {{ session('message') }}</div>
                        </div>
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <div class="table-responsive">
                <table class="table mb-0 align-middle">
                    <thead>
                        <tr>
                            <th>#ល.រ</th>
                            <th>រូបតំណាង</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះ​កំរិតសិក្សា</th>
                            <th>ស្ថានភាព</th>
                            <th>សាលា</th>
                            <th>អ្នកបញ្ចូល</th>

                            <th>សកម្មភាព</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($levels as $level)
                            <tr>
                                <td>{{$level->id}}</td>
                                <td>
                                    <div class="product-img bg-transparent border">
                                        @if (Storage::disk('public')->exists($level->image))
                                            <img src="{{asset('storage/'.$level->image)}}" class="p-1" alt="">
                                        @else
                                            <img src="{{asset('assets/images/icons/shoes.png')}}" class="p-1" alt="">
                                        @endif
                                    </div>
                                </td>
                                <td>{{$level->code}}</td>
                                <td>
                                    {{$level->name}}<br>
                                    {{$level->description}}
                                </td>
                                <td>
                                    <span class="btn btn-sm btn-{{$level->status==0?'danger':'success'}} radius-30" wire:click="setStatus({{$level->id}},{{$level->status}})">
                                        {{$level->status==1?'អោយប្រើ':'មិនអោយប្រើ'}}
                                    </span>
                                </td>
                                <td>{{getName("schools","name",$level->school_id)}}</td>
                                <td><span class="fa fa-user"></span> {{getName("users","name",$level->user_id)}}</td>
                                <td>
                                    {{-- <button data-toggle="modal"  data-bs-toggle="modal" data-bs-target="#updateModal" wire:click="edit({{ $level->id }})" class="btn btn-primary btn-sm">Edit</button> --}}

                                    <a href="javaScript:;" data-bs-toggle="modal" data-bs-target="#updateModal" wire:click="edit({{ $level->id }})" class="btn btn-sm btn-info radius-30 text-white">
                                        <span class="fa fa-edit"></span> កែប្រែ</a>
                                    <a href="javaScript:;" data-bs-toggle="modal" data-bs-target="#deleteModal" wire:click="formDelete({{ $level->id }})" class="btn btn-sm btn-danger radius-30"><span class="fa fa-trash"></span> លុប</a>
                                </td>
                            </tr>
                        @endforeach


                    </tbody>
                </table>
            </div>

            <div class="pull-right mt-3"> {{ $levels->links() }}</div>
        </div>
    </div>





</div>


@push('scripts')
<script type="text/javascript">
    window.livewire.on('userStore', () => {
        $('#CreateModal').modal('hide');
        $('#updateModal').modal('hide');
        $('#deleteModal').modal('hide');
    });
</script>


@endpush
