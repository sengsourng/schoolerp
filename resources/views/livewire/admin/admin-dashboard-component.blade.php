<div>

    <div class="row row-cols-1 row-cols-lg-4">
        <div class="col">
            <div class="card radius-10 overflow-hidden bg-gradient-cosmic">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h4 class="mb-0 text-white pb-2">មុខវិជ្ជាសរុប</h4>
                            <h3 class="mb-0 text-white">867</h3>
                        </div>
                        <div class="ms-auto text-white">
                            {{-- online-lesson --}}
                            {{-- <i class="fa fa-suitcase font-60"></i> --}}
                            <span><img style="height: 64px;" src="{{asset('assets/images/icons/online-lesson.png')}}" alt=""></span>

                        </div>
                    </div>
                    <div class="progress bg-white-2 radius-10 mt-2" style="height:4.5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 46%"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 overflow-hidden bg-gradient-Ohhappiness">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h4 class="mb-0 text-white pb-2">មេរៀនសរុប</h4>
                            <h3 class="mb-0 text-white">540 <small>មេរៀន</small></h3>
                        </div>
                        <div class="ms-auto text-white">
                            {{-- <i class='fa fa-book font-60'></i> --}}
                            <span><img style="height: 64px;" src="{{asset('assets/images/icons/lesson.png')}}" alt=""></span>

                        </div>
                    </div>
                    <div class="progress bg-white-2 radius-10 mt-2" style="height:4.5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 72%"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 overflow-hidden"
            style="background: linear-gradient(to right, rgb(71, 88, 238), rgb(3, 171, 212)) !important;"
            >
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h4 class="mb-0 text-white pb-2">សាស្រ្តាចារ្យសរុប</h4>
                            <h3 class="mb-0 text-white">64 <small>នាក់</small></h3>
                        </div>
                        <div class="ms-auto text-white">
                            {{-- <i class='fa fa-users font-60'></i> --}}
                            <span><img style="height: 64px;" src="{{asset('assets/images/icons/teacher.png')}}" alt=""></span>


                        </div>
                    </div>
                    <div class="progress bg-white-2 radius-10 mt-2" style="height:4.5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 68%"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10 overflow-hidden">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h4 class="mb-0 text-blue pb-2">សិស្សសរុប</h4>
                            <h3 class="mb-0 text-success">130 <small>នាក់</small></h3>
                        </div>
                        <div class="ms-auto text-white">
                            {{-- <i class='fa fa-chalkboard-teacher font-60'></i> --}}
                            <span><img style="height: 64px;" src="{{asset('assets/images/icons/students.png')}}" alt=""></span>

                        </div>
                    </div>
                    <div class="progress  bg-white-2 radius-10 mt-2" style="height:4.5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 66%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--end row-->



    <div class="row">
        <div class="col-md-4">
            <div class="card radius-10 w-100 h-300">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div>
                            <h3 class="font-weight-bold mb-0">មុខវិជ្ជាដែលសិស្សកំពុងរៀន</h3>
                        </div>
                        <div class="dropdown ms-auto">
                            <div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown">
                                <i class="bx bx-dots-horizontal-rounded"></i>
                            </div>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javaScript:;">បន្ថែមមុខវិជ្ជា</a>
                                <a class="dropdown-item" href="javaScript:;">បន្ថែមមេរៀន</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javaScript:;">គ្រូបង្រៀនតាមមុខវិជ្ជា</a>
                            </div>
                        </div>
                    </div>
                   </div>
                    <div class="best-selling-products p-3 mb-3 ps ps--active-y">
                        @for ($i = 1; $i < 10; $i++)
                            <div class="d-flex align-items-center pb-3">
                                <div class="product-img">
                                    <img src="{{asset('assets/images/icons/online-lesson.png')}}" class="p-1" alt="">
                                </div>
                                <div class="ps-3">
                                    <h6 class="mb-0 font-weight-bold">C++ Programming</h6>
                                    <p class="mb-0 text-secondary">ជំនាញ វិទ្យាសាស្រ្តកុំព្យូទ័រ</p>
                                </div>
                                <p class="ms-auto mb-0 text-purple"><i class="bx bxs-user text-primary mr-1"></i> 25 នាក់</p>
                            </div>

                        @endfor

                    <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 250px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 288px;"></div></div></div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="col d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="font-weight-bold mb-0">ជំនាញដែលសិស្សកំពុងរៀន</h3>
                            </div>
                            <div class="dropdown ms-auto">
                                <div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded"></i>
                                </div>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javaScript:;">ព្រីន</a>
                                    <a class="dropdown-item" href="javaScript:;">យកជា Excel</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javaScript:;">យកជា PDF</a>
                                </div>
                            </div>
                        </div>
                      </div>
                        <div class="recent-reviews p-3 mb-3 ps ps--active-y">
                            @for ($i = 1; $i < 10; $i++)
                                <div class="d-flex align-items-center pb-3">
                                    <div class="product-img">
                                        <img src="{{asset('assets/images/icons/reading.png')}}" class="p-1" alt="">
                                    </div>
                                    <div class="ps-3">
                                        <h6 class="mb-0 font-weight-bold">ជំនាញ វិទ្យាសាស្រ្តកុំព្យូទ័រ</h6>
                                        <p class="mb-0 text-secondary">ជំនាន់ទី៩ ឆមាសទី១</p>
                                    </div>
                                    <p class="ms-auto mb-0"><i class="bx bxs-user text-success mr-1"></i> 50 នាក់</p>
                                </div>
                            @endfor

                        <div class="ps__rail-x" style="left: 0px; bottom: -173px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 173px; height: 450px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 125px; height: 325px;"></div></div></div>

                </div>
               </div>
        </div>

        <div class="col-md-4">
            <div class="col d-flex">
                <div class="card radius-10 w-100">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="font-weight-bold mb-0">សិស្សដែលអវត្តមាន</h3>
                            </div>
                            <div class="dropdown ms-auto">
                                <div class="cursor-pointer text-dark font-24 dropdown-toggle dropdown-toggle-nocaret" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded"></i>
                                </div>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javaScript:;">ព្រីន</a>
                                    <a class="dropdown-item" href="javaScript:;">យកជា Excel</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javaScript:;">យកជា PDF</a>
                                </div>
                            </div>
                         </div>
                        </div>
                        <div class="support-list p-3 mb-3 ps ps--active-y">
                            @for ($i = 1; $i < 10; $i++)
                                <div class="d-flex align-items-top pb-3">
                                    <div class="">
                                        <img src="{{asset('assets/images/icons/student.png')}}" width="45" height="45" class="rounded-circle" alt="">
                                    </div>
                                    <div class="ps-2">
                                        <h6 class="mb-1 font-weight-bold">សេង ដារា <span class="text-primary float-end font-13">2 hours ago</span></h6>
                                        <p class="mb-0 font-13 text-secondary">ជំនាញ វិទ្យាសាស្រ្ត​កុំព្យូទ័រ ជំនាន់​៩ ឆមាស ទី២</p>
                                    </div>
                                </div>
                            @endfor

                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 450px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 269px;"></div></div></div>

                </div>
               </div>
        </div>


    </div>




</div>
