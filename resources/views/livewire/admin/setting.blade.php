<div>
    <div class="row row-cols-1 row-cols-lg-3">
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">

                            <div class="flex-grow-1">
                                <p class="mb-0">គ្រឹះស្ថានសិក្សា</p>
                                <h4 class="font-weight-bold">32,842 <small class="text-success font-13">(សាលា)</small></h4>
                                <p class="text-success mb-0 font-13">សិស្សសរុប ២៤០នាក់ ស្រី ១០០នាក់</p>
                            </div>
                            <a href="{{route('admin.schools')}}">
                                <div class="widgets-icons bg-gradient-cosmic text-white">
                                    <i class="bx bx-refresh"></i>
                                </div>
                            </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <p class="mb-0">អ្នកប្រើប្រាស់</p>
                            <h4 class="font-weight-bold">16,352 <small class="text-success font-13">(+22%)</small></h4>
                            <p class="text-secondary mb-0 font-13">អ្នកប្រើប្រាស់ប្រាំសប្តាហ៍</p>
                        </div>
                        <div class="widgets-icons bg-gradient-burning text-white"><i class="bx bx-group"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <p class="mb-0">ឆមាស</p>
                            <h4 class="font-weight-bold">34 <small class="text-success font-13">(ឆមាស)</small></h4>
                            <p class="text-secondary mb-0 font-13">ចំនួនឆមាសក្នុងសាលា</p>
                        </div>
                        <div class="widgets-icons bg-gradient-lush text-white"><i class="bx bx-time"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <p class="mb-0">មុខវិជ្ជា</p>
                            <h4 class="font-weight-bold">1,94,2335</h4>
                            <p class="text-secondary mb-0 font-13">ចំនួនមុខវិជ្ជាដែលមាន</p>
                        </div>
                        <div class="widgets-icons bg-gradient-kyoto text-white"><i class="bx bxs-cube"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <p class="mb-0">កំរិតសិក្សា</p>
                            <h4 class="font-weight-bold">30 <small class="text-danger font-13">(កំរិត)</small></h4>
                            <p class="text-secondary mb-0 font-13">កំរិតសិក្សា</p>
                        </div>
                        <div class="widgets-icons bg-gradient-blues text-white"><i class="bx bx-line-chart"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card radius-10">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="flex-grow-1">
                            <p class="mb-0">ជំនាន់</p>
                            <h4 class="font-weight-bold">10 <small class="text-danger font-13">(ជំនាន់)</small></h4>
                            <p class="text-secondary mb-0 font-13">ចំនួនជំនាន់</p>
                        </div>
                        <div class="widgets-icons bg-gradient-moonlit text-white"><i class="bx bx-bar-chart"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
