
<div>
    <div class="card radius-10">
        <div class="card-header border-bottom-0 bg-transparent pt-3">
            <div class="d-flex align-items-center">
                <div>
                    <h4 class="font-weight-bold mb-0">បញ្ជីឈ្មោះសាលា</h4>
                </div>
                <div class="ms-auto">

                    <button type="button" class="btn btn-success radius-10" data-bs-toggle="modal" data-bs-target="#CreateModal"><span class="fa fa-plus"></span> បន្ថែមថ្មី</button>

                </div>
            </div>
        </div>
        <div class="card-body">
            @include('livewire.admin.school.create')
            @include('livewire.admin.school.update')
            @include('livewire.admin.school.delete')

            @if (session()->has('message'))
                <div class="alert alert-success" style="margin-top:30px;">x
                {{ session('message') }}
                </div>
            @endif

            <div class="table-responsive">
                <table class="table mb-0 align-middle">
                    <thead>
                        <tr>
                            <th>#ល.រ</th>
                            <th>រូបតំណាង</th>
                            <th>ឈ្មោះសាលា</th>
                            <th>ឈ្មោះខ្លី</th>
                            <th>នាយក/នាយិកា</th>
                            <th>ទូរស័ព្ទ/អ៊ីម៉ែល</th>
                            <th>អាស័យដ្ឋាន</th>
                            <th>ស្ថានភាព</th>
                            <th>សកម្មភាព</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($schools as $school)
                            <tr>
                                <td>{{$school->id}}</td>

                                <td>
                                    <div class="product-img bg-transparent border">
                                        @if (Storage::disk('public')->exists($school->logo))
                                            <img src="{{asset('storage/'.$school->logo)}}" class="p-1" alt="">
                                        @else
                                            <img src="{{asset('assets/images/icons/shoes.png')}}" class="p-1" alt="">
                                        @endif
                                    </div>
                                </td>
                                <td>{{$school->name}}</td>
                                <td>{{$school->short_name}}</td>
                                <td>{{$school->director_name}}</td>
                                <td>{{$school->phone}}<br>{{$school->email}}</td>
                                <td>{{$school->address}}</td>
                                <td>
                                    <a href="javaScript:;" class="btn btn-sm btn-{{$school->user_id==null?'danger':'success'}} radius-30"><span class="fa fa-user"></span> {{$school->checkUser($school->user_id)}}</a></td>
                                <td>
                                    {{-- <button data-toggle="modal"  data-bs-toggle="modal" data-bs-target="#updateModal" wire:click="edit({{ $school->id }})" class="btn btn-primary btn-sm">Edit</button> --}}

                                    <a href="javaScript:;" data-bs-toggle="modal" data-bs-target="#updateModal" wire:click="edit({{ $school->id }})" class="btn btn-sm btn-info radius-30 text-white">
                                        <span class="fa fa-edit"></span> កែប្រែ</a>
                                    <a href="javaScript:;" data-bs-toggle="modal" data-bs-target="#deleteModal" wire:click="formDelete({{ $school->id }})" class="btn btn-sm btn-danger radius-30"><span class="fa fa-trash"></span> លុប</a>
                                </td>
                            </tr>
                        @endforeach


                    </tbody>
                </table>
            </div>

            <div class="pull-right mt-3"> {{ $schools->links() }}</div>
        </div>
    </div>





</div>


@push('scripts')
<script type="text/javascript">
    window.livewire.on('userStore', () => {
        $('#CreateModal').modal('hide');
        $('#updateModal').modal('hide');
        $('#deleteModal').modal('hide');
    });
</script>


@endpush
