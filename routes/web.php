<?php

use App\Http\Livewire\Admin\AdminDashboardComponent;
use App\Http\Livewire\Admin\AdminProfileComponent;
use App\Http\Livewire\Admin\LevelComponent;
use App\Http\Livewire\Admin\SchoolComponent;
use App\Http\Livewire\Admin\Setting;
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\Manager\ManagerDashboardComponent;
use App\Http\Livewire\Student\StudentDashboardComponent;
use App\Http\Livewire\Teacher\TeacherDashboardComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',HomeComponent::class)->name('home');



Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// សម្រាប់អសិស្ស​ និស្សិត For Student
Route::middleware(['auth:sanctum', 'verified','authstudent'])->group(function(){
    Route::get('/student/dashboard',StudentDashboardComponent::class)->name('student.dashboard');
    Route::get('/student/profile/{user_id}',AdminProfileComponent::class)->name('student.profile');
});


// សម្រាប់អ្នកគ្រប់គ្រង For Manager
Route::middleware(['auth:sanctum', 'verified','authmanager'])->group(function(){
    Route::get('/manager/dashboard',ManagerDashboardComponent::class)->name('manager.dashboard');
    Route::get('/manager/profile/{user_id}',AdminProfileComponent::class)->name('manager.profile');


});


// សម្រាប់គ្រូបង្រៀន For Teacher
Route::middleware(['auth:sanctum', 'verified','authteacher'])->group(function(){
    Route::get('/teacher/dashboard',TeacherDashboardComponent::class)->name('teacher.dashboard');
    Route::get('/teacher/profile/{user_id}',AdminProfileComponent::class)->name('teacher.profile');


});


// សម្រាប់រដ្ឋបាល For Admin
Route::middleware(['auth:sanctum', 'verified','authadmin'])->group(function(){
    Route::get('/admin/dashboard',AdminDashboardComponent::class)->name('admin.dashboard');
    Route::get('/admin/profile/{user_id}',AdminProfileComponent::class)->name('admin.profile');

    // Schools
    Route::get('/admin/schools',SchoolComponent::class)->name('admin.schools');
    Route::get('/admin/levels',LevelComponent::class)->name('admin.levels');
    Route::get('/admin/settings',Setting::class)->name('admin.seetings');

    // // Service Categories
    // Route::get('/admin/service-categories',AdminServiceCategoryComponent::class)->name('admin.service_categories');
    // Route::get('/admin/service-categories/add',AdminAddServiceCategoryComponent::class)->name('admin.add_service_categories');
    // Route::get('/admin/service-categories/edit/{category_id}',AdminServiceCategoryEditComponent::class)->name('admin.service_category_edit');

    // // Services
    // Route::get('/admin/all-services',AdminServicesComponent::class)->name('admin.all_services');
    // Route::get('/admin/service/add',AdminServiceAddComponent::class)->name('admin.add_service');

    // Route::get('/admin/{category_slug}/services',AdminServicesByCategoryComponent::class)->name('admin.services_by_category');
    // Route::get('/admin/service/edit/{service_id}',AdminServiceEditComponent::class)->name('admin.service_edit');

});

